# main.py

from flask import Blueprint, render_template
from flask_login import login_required, current_user
from flask_table import Table, Col
from . import db
from .models import User, Commands
import pickle

main = Blueprint('main', __name__)


class ItemTable(Table):
    command = Col('Command')


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/profile')
@login_required
def profile():
    return render_template('profile.html', name=current_user.name)


@main.route('/showcommands')
@login_required
def show_commands():
    user_id = User.query.filter_by(email=current_user.email).first()
    out = Commands.query.filter_by(user_id=user_id.id).all()
    table = []
    try:
        for x in out:
            try:
                table.append({"command":pickle.loads(x.command.replace(b'\\n', b'\n'))})
            except:
                table.append({"command": x.command.decode()})
    except:
        table = ItemTable(table)
    return render_template('show_commands.html', name=current_user.name, table=ItemTable(table))

